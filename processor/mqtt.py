import os

import paho.mqtt.client as mqtt


def _log(msg):
    print('MQTT: %s' % msg)


def on_log(client, userdata, level, buf):
    if level == mqtt.MQTT_LOG_ERR or level == mqtt.MQTT_LOG_ERR:
        _log(buf)


def on_disconnect(client, userdata, rc):
    _log('disconnected!')


def on_connect(client, userdata, flags, rc):
    if rc != mqtt.CONNACK_ACCEPTED:
        _log('connection failed')
        os._exit(1)

    _log('connected!')


def create_client(config_provider):
    client = mqtt.Client(client_id=config_provider.get('client_id', 'trekkerdata'))
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_log = on_log
    client.reconnect_delay_set(min_delay=1, max_delay=120)
    client.username_pw_set(
        config_provider.get('user'),
        config_provider.get('pass')
    )
    client.connect(
        config_provider.get('host'),
        config_provider.get('port', 1883),
        config_provider.get('timeout', 60)
    )

    client.loop_start()

    return client
