# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Install python mysql connector
RUN apt-get update && apt-get install -y \
    python3.7-dev \
    default-libmysqlclient-dev

COPY requirements.txt /tmp/
RUN pip install --upgrade pip
RUN pip install --trusted-host pypi.python.org -r /tmp/requirements.txt

# Set the working directory to /app
WORKDIR /root

# Copy the current directory contents into the container at /app
COPY . /root

ENTRYPOINT ["python", "-m"]
