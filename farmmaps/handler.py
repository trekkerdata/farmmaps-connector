import time

from . import farmmaps_pb2

META_DATA_URL = 'http://192.168.0.1:8080/v1/meta/sensors'
BASE_DEV_URN = 'urn:dev:nl.tractordata:%s'


class Handler:
    def __init__(self, stats, mqtt_client, mqtt_topic):
        self.stats = stats
        self.should_add_meta_data_url = True
        self.mqtt_client = mqtt_client
        self.mqtt_topic = mqtt_topic

    def send_meta_data_url_with_next_message(self):
        self.should_add_meta_data_url = True

    def handle(self, messages):
        data = {}
        for message in messages:
            if message['data'] is not None:
                data[message['data']['key']] = message['data']['val']

        self.send_message(
            self.create_protobuf_msg(messages[0], data)
        )

        self.stats.increase(messages[0]['mid'])
        self.stats.increase('sent')

        time.sleep(0.5)

    def send_message(self, message):
        self.mqtt_client.publish(self.mqtt_topic, message.SerializeToString(), qos=2).wait_for_publish()

    def create_protobuf_msg(self, message, data):
        msg = farmmaps_pb2.DataPoint()
        msg.machine_id = BASE_DEV_URN % (message['mid'])
        msg.ts = message['ts']
        msg.lon = message['lon']
        msg.lat = message['lat']

        if message['alt']:
            msg.altitude = message['alt']

        if message['hdg']:
            msg.heading = message['hdg']

        if message['spd']:
            msg.speed = message['spd']

        for key, value in data.items():
            measurement = msg.sensors.add()
            measurement.key = key
            measurement.value = value

        if self.should_add_meta_data_url:
            msg.metadata_url = META_DATA_URL
            self.should_add_meta_data_url = False
            self.stats.increase('metadata')

        return msg
