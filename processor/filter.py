import json


class ParsePayload:
    """Iterator that parses kafka json message"""

    def __init__(self, parent):
        self.parent = parent

    def __iter__(self):
        return self

    def __next__(self):
        for message in self.parent:
            payload = json.loads(message.value)
            payload['meta'] = {
                'offset': message.offset
            }
            return payload


class ByMachineId:
    """Iterator that selects messages that match given machine id"""

    def __init__(self, parent, machine_ids):
        self.parent = parent
        self.machine_ids = machine_ids

    def __iter__(self):
        return self

    def __next__(self):
        for message in self.parent:
            if message['mid'] in self.machine_ids:
                return message
