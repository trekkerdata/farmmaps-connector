class ByPropertyValue:
    """Iterator that selects messages that match given machine id"""

    def __init__(self, parent, properties, max_msgs):
        self.parent = parent
        self.properties = properties
        self.max_msgs = max_msgs
        self.aggregated_messages = []

    def __iter__(self):
        return self

    def __next__(self):
        for message in self.parent:
            if not self.aggregated_messages:
                self.aggregated_messages.append(message)
                continue

            if self.should_aggregate(message):
                self.aggregated_messages.append(message)
                continue

            # release aggregated values and reset
            aggregated = self.aggregated_messages
            self.aggregated_messages = [message]

            return aggregated

    def should_aggregate(self, message):
        if len(self.aggregated_messages) > self.max_msgs:
            return False

        for prop in self.properties:
            if message[prop] != self.aggregated_messages[0][prop]:
                return False

        return True
