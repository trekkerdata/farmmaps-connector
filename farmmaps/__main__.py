from farmmaps import main

if __name__ == '__main__':
    try:
        main.run()
    except KeyboardInterrupt:
        exit(0)
