from kafka import KafkaConsumer


def create_consumer(config_provider):
    return KafkaConsumer(
        config_provider.get('topic', 'decoded'),
        bootstrap_servers=config_provider.get('broker', 'broker'),
        client_id=config_provider.get('client_id', 'emitter'),
        group_id=config_provider.get('group_id'),
        enable_auto_commit=True,
        auto_commit_interval_ms=1000,
        api_version=(0, 10, 1),
        auto_offset_reset=config_provider.get('offset', 'latest'),
    )
