import time
from datetime import datetime


class Stats:
    def __init__(self, pulse):
        self.ts_last = time.time()
        self.pulse = pulse
        self.stats = {}

    def set(self, name, value):
        self.stats[name] = value
        self.write_stats_intermittent()

    def increase(self, name):
        if name not in self.stats:
            self.stats[name] = 0

        self.stats[name] += 1
        self.write_stats_intermittent()

    def write_stats_intermittent(self):
        now = time.time()
        if now - self.ts_last > self.pulse:
            print(self, flush=True)
            self.ts_last = now
            self.reset()

    def reset(self):
        self.stats = {}

    def __str__(self):
        return datetime.now().isoformat() + ': ' + str(self.stats)
