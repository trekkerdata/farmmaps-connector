#!/usr/bin/env python

from farmmaps.handler import Handler as FarmMapsHandler
from processor import aggregate, kafka, filter, config, mqtt, processor
from processor.stats import Stats
from . import jobs


# main entry
def run():
    stats = Stats(pulse=2)

    mqtt_client = mqtt.create_client(config.Env('FARMMAPS'))
    kafka_consumer = kafka.create_consumer(config.Env('SOURCE'))

    iterable = filter.ParsePayload(kafka_consumer)
    iterable = filter.ByMachineId(iterable, ['ib0XX'])
    iterable = aggregate.ByPropertyValue(iterable, ['mid', 'ts'], max_msgs=10)

    handler = FarmMapsHandler(stats, mqtt_client, config.Env('FARMMAPS').get('topic', 'trekkerdata/sensors'))

    jobs.start(handler)
    processor.iterate(iterable, handler)
