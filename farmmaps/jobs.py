from datetime import timedelta
from timeloop import Timeloop

_handler = None
tl = Timeloop()


@tl.job(interval=timedelta(seconds=60))
def send_meta_data_url_with_next_message():
    global _handler
    _handler.send_meta_data_url_with_next_message()


def start(handler):
    global _handler
    _handler = handler
    tl.start()
