def iterate(messages, handler):
    print("waiting for messages...")
    for message in messages:
        handler.handle(message)
