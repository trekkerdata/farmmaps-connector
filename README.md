# Connectors

This module provides connectors to third party data consumers like Farmmaps and Farm24.  

## Configuration

The following settings are available for all connectors: 

| VAR                | Default value  | Description | 
|---------           |-------------   |------       | 
| `SOURCE_BROKER`    | `broker`    | `hostname:port` of the source broker, eg `localhost:9092`
| `SOURCE_CLIENT_ID` | `connector`    | The client ID used by the consumer
| `SOURCE_GROUP_ID`  |                | The group ID used by the consumer. Required, pick a unique name for each connector.
| `SOURCE_TOPIC`     | `decoded`      | The topic to consume messages from
| `SOURCE_OFFSET`    | `latest`       | The Kafka offset at which the consumer will initially start consuming messages. Once started it will always continue at the last consumed offset. 

The following settings are available for FARMMAPS connector:
 
| VAR                  | Default value       | Description | 
|---------             |-------------        |------       | 
| `FARMMAPS_HOST`      |                     | Host address of the MQTT endpoint. Required.
| `FARMMAPS_USER`      |                     | Username of the MQTT endpoint. Required.
| `FARMMAPS_PASS`      |                     | Password of the MQTT endpoint. Required.
| `FARMMAPS_PORT`      | 1883                | Port of the FarmMaps MQTT endpoint. Required.
| `FARMMAPS_TOPIC`     | trekkerdata/sensors | Topic onto which to publish MQTT messages.
| `FARMMAPS_KEEPALIVE` | 60                  | Time in seconds between sending PINGs to MQTT


## Deployment

Example:
```
$ SOURCE_BROKER=localhost FARMMAPS_HOST=farmmaps.awtest.nl python -m farmmaps
```

## Local dev
Create a virtualenv with python 3+, then run a module:
```bash
$ virtualenv --python=/usr/local/bin/python3 .
$ source bin/activate
$ pip install -r requirements.txt
$ SOURCE_BROKER=td-main python -m farmmaps
``` 
 
## Generating protobuf classes
After changing the farmmaps.proto (protobuf) message definition, run the following command 
to regenerate the serialization code:  
```bash
protoc -I=farmmaps --python_out=farmmaps farmmaps/farmmaps.proto 
```

The `protoc` binary can be downloaded from: https://github.com/protocolbuffers/protobuf/releases/tag/v3.10.1
