import os


class Env:
    def __init__(self, prefix):
        self.prefix = prefix

    def get(self, name, default = None):
        env_key = '%s_%s' % (self.prefix.upper(), name.upper())
        env_value = os.getenv(env_key, default)

        if env_value is None:
            raise EnvironmentError('%s environment variable must be set' % env_key)

        return env_value
